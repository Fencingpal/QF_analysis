#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import re
import os
import pandas as pd
import mendeleev as md

def make_data_file(directory):
    name=str(directory)+'/for006'
  
    #setup preliminary values
    file=open(name).readlines()
    databox=np.zeros((50000,15))
    #columns are:time,totmass,totcharge,totcm,fragdist,neck,frag1q,frag1m,frag1v,frag2q,frag2m,frag2v,coulomb,kinetic,nof
    dataline=0
    neckcount=0
    count=len(file)
    fragno=0
    lineno=0
    while lineno<count:
        line=file[lineno]

        if re.search("Neck density",line):        
            if neckcount==0:
                neckcount=1
            elif neckcount==1:
                databox[dataline,5]=float(line.split()[2])
                neckcount=0
        elif re.search("Number of fragments",line):        
            if neckcount==1:
                databox[dataline,14]=float(line.split()[3])      
        elif re.search("Time:",line):          
            databox[dataline,0]=float(line.split()[1])
            databox[dataline,4]=float(line.split()[4])
        elif re.search("Fragment # 0",line): 
            fragno=0        
        elif re.search("Fragment # 1",line): 
            fragno=1 
        elif re.search("Fragment # 2",line): 
            fragno=2
        elif (re.search("c.m.=",line)!=None)&(fragno==0): 
            databox[dataline,1]=float(line.split()[5])
            databox[dataline,2]=float(line.split()[7])
            databox[dataline,3]=float(line.split()[3])
        elif (re.search("c.m.=",line)!=None)&(fragno==1): 
            databox[dataline,6]=float(line.split()[7])    
            databox[dataline,7]=float(line.split()[5])
        elif (re.search("c.m.=",line)!=None)&(fragno==2): 
            databox[dataline,9]=float(line.split()[7])    
            databox[dataline,10]=float(line.split()[5])
        elif (re.search("Velocity",line)!=None)&(fragno==1): 
            databox[dataline,8]=float(line.split()[4])
        elif (re.search("Velocity",line)!=None)&(fragno==2): 
            databox[dataline,11]=float(line.split()[4])
        elif re.search("Relative kinetic energy",line):
            databox[dataline,13]=float(line.split()[3])
        elif re.search("Coulomb interaction energy:",line): 
            databox[dataline,12]=float(line.split()[3])
            if databox[dataline,7]>databox[dataline,10]:
                databox[dataline,6], databox[dataline,9]=databox[dataline,9], databox[dataline,6]
                databox[dataline,7], databox[dataline,10]=databox[dataline,10], databox[dataline,7]
                databox[dataline,8], databox[dataline,11]=databox[dataline,11], databox[dataline,8]
            dataline+=1
        lineno+=1    
        
    databox=databox[databox[:,1]>0]        #clear all the empty rows of the array
    databox[:,0]=databox[:,0]*10**(6)/299792458
    initdist=databox[0,4]
    databox=databox[databox[:,4]<=initdist]
    databox=pd.DataFrame(databox)
    headers=['time','totmass','totcharge','totcm','fragdist','neck','frag1q','frag1m','frag1v','frag2q','frag2m','frag2v','coulomb','kinetic','nof']
    databox.columns=headers
    
    return(databox)   

def run_details(data):
    runen=round(data.coulomb[0]+data.kinetic[0],2)
    global z_0
    global n_0
    lightfragq=int(round(data.frag1q[0]))
    z_0=lightfragq
    lightfragm=int(round(data.frag1m[0]))
    n_0=lightfragm-lightfragq
    heavyfragq=int(round(data.frag2q[0]))
    heavyfragm=int(round(data.frag2m[0]))
    compq=lightfragq+heavyfragq
    compm=lightfragm+heavyfragm
    lightname=md.element(lightfragq).symbol
    heavyname=md.element(heavyfragq).symbol
    compname=md.element(compq).symbol
    title=lightname+str(lightfragm)+' on '+heavyname+str(heavyfragm)+" at "+str(runen)+' MeV'
    print(lightname+str(lightfragm)+' on '+heavyname+str(heavyfragm)+' (compound nucleus '+compname+str(compm)+')')
    print('Run at energy '+str(runen)+' MeV')
    last=len(data.time)-10
    lightfragq=int(round(data.frag1q[last]))
    lightfragm=int(round(data.frag1m[last]))
    heavyfragq=int(round(data.frag2q[last]))
    heavyfragm=int(round(data.frag2m[last]))
    lightname=md.element(lightfragq).symbol
    heavyname=md.element(heavyfragq).symbol
    compname=md.element(compq).symbol
    finalen=round(data.coulomb[last]+data.kinetic[last],2)
    print('Final fragments are: '+lightname+str(lightfragm)+' and '+heavyname+str(heavyfragm))
    print('TKE: '+str(finalen)+' MeV')
    
    tottime=data.time[last]
    print('Total time '+str(tottime)+' zeptoseconds')
    if abs(data.coulomb[last-100]+data.kinetic[last-100]-data.coulomb[last]-data.kinetic[last])>0.25:
        print('Final fragments are not separated')
    
    contact=0
    dt=0.2*10**(6)/299792458
    for i in range(len(data.time)):
        if data.neck[i]>=0.08:
            contact+=dt
    
    print('Contact time: '+str(round(contact,3))+'zs')
    print('')
    return(title)

def save_and_plot(data,name):
    title=run_details(data)
    fname="QF_data_"+name.replace("_8.dir",".csv")
    data.to_csv(fname)
    plt.clf()
    fig,ax=plt.subplots(2,2,figsize=(16,10))
    ax[0,0].plot(data.time,data.fragdist)
    ax[0,1].plot(data.time,data.neck)
    ax[0,1].plot(data.time,data.time/data.time*0.08,'--')
    
    ax[1,0].plot(data.time,data.frag1m)
    ax[1,0].plot(data.time,data.frag1q)
    ax[1,0].plot(data.time,data.frag1m-data.frag1q)
    ax[1,1].plot(data.time,data.coulomb)
    ax[1,1].plot(data.time,data.kinetic)
    ax[1,1].plot(data.time,data.coulomb+data.kinetic)
    
    ax[0,0].set_xlabel("Time (zs)",fontsize=16)
    ax[0,1].set_xlabel("Time (zs)",fontsize=16)
    ax[1,0].set_xlabel("Time (zs)",fontsize=16)
    ax[1,1].set_xlabel("Time (zs)",fontsize=16)
    ax[0,0].set_ylabel("Fragment distance (fm)",fontsize=16)
    ax[0,1].set_ylabel("Neck density (nucleons/$fm^3$)",fontsize=16)
    ax[1,0].set_ylabel("Nucleon number (light fragment)",fontsize=16)
    ax[1,1].set_ylabel("Energy (MeV)",fontsize=16)
    ax[1,0].legend(['Total','Proton','Neutron'],fontsize=16)
    ax[1,1].legend(['Coulomb','Kinetic','Total'],fontsize=16)
    fig.suptitle(title,fontsize=32)
    ax[0,0].tick_params(axis='x',labelsize=16)
    ax[0,0].tick_params(axis='y',labelsize=16)
    ax[0,1].tick_params(axis='x',labelsize=16)
    ax[0,1].tick_params(axis='y',labelsize=16)
    ax[1,0].tick_params(axis='x',labelsize=16)
    ax[1,0].tick_params(axis='y',labelsize=16)
    ax[1,1].tick_params(axis='x',labelsize=16)
    ax[1,1].tick_params(axis='y',labelsize=16)
    plt.savefig(name.replace("_8.dir",".pdf"))
    
def collect_all_data():
    out=open("QF_runs.txt","w")
    out.write("ECM tau Z_H N_H Z_L N_L TKE"+"\n")
    out.close()
    for name in os.listdir():
        if (os.path.isfile(name+'/for006')):
            data=make_data_file(name)
            save_and_plot(data,name)
            runen=round(data.coulomb[0]+data.kinetic[0],2)       
            last=len(data.time)-10
            lightfragq=round(data.frag1q[last],2)
            lightfragn=round(data.frag1m[last]-data.frag1q[last],2)
            heavyfragq=round(data.frag2q[last],2)
            heavyfragn=round(data.frag2m[last]-data.frag2q[last],2)
            finalen=round(data.coulomb[last]+data.kinetic[last],2)
            contact=0
            dt=0.2*10**(6)/299792458
            for i in range(len(data.time)):
                if data.neck[i]>=0.08:
                    contact+=dt
            if runen<300:
                out=open("QF_runs.txt","a")
                out.write(str(runen)+' '+str(contact)+' '+str(heavyfragq)+' '+str(heavyfragn)+' '+str(lightfragq)+' '+str(lightfragn)+' '+str(finalen)+"\n")
                out.close()

collect_all_data()

global_dat=pd.read_csv("QF_runs.txt",sep=" ")
global_dat.head()
plt.clf()
plt.plot(global_dat.ECM,global_dat.tau,'o')
plt.xlabel("Center of mass energy (MeV)")
plt.ylabel("Contact time (zs)")
plt.savefig("contact_time.pdf")

plt.clf()
plt.plot(global_dat.ECM,global_dat.Z_L,'ro')
plt.plot(global_dat.ECM,global_dat.ECM/global_dat.ECM*20,'r:')
plt.plot(global_dat.ECM,global_dat.N_L,'b+')
plt.plot(global_dat.ECM,global_dat.ECM/global_dat.ECM*28,'b:')
plt.xlabel("Center of mass energy (MeV)")
plt.ylabel("Nucleon number of light fragment")
plt.legend(["Proton final","Proton original","Neutron final","Neutron original"])
plt.savefig("transfer.pdf")

plt.clf()
plt.plot(global_dat.ECM,global_dat.TKE,'o')
plt.xlabel("Center of mass energy (MeV)")
plt.ylabel("Total kinetic energy (MeV)")
plt.savefig("TKE.pdf")

plt.clf()
plt.plot(global_dat.tau,global_dat.Z_L-z_0,'ro')
plt.plot(global_dat.tau,global_dat.N_L-n_0,'bo')
plt.xlabel("Contact time (zs)")
plt.ylabel("Transferred particles")
plt.legend(["Protons","Neutrons"])
plt.savefig("rate.pdf")
